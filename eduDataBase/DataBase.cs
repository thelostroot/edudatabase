﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eduDataBase
{
    public class DataBase
    {
        List<Table> _tables;
        public DataBase()
        {
            _tables = new List<Table>();
        }

        public void AddTable(Table t)
        {
            _tables.Add(t);
        }

        public bool DeleteTable(string name)
        {
            for (int i = 0; i < _tables.Count; i++)
            {
                if (_tables[i].Name == name)
                {
                    _tables.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

    }
}
