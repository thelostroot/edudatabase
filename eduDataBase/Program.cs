﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace eduDataBase
{
    class Program
    {
        static void Main(string[] args)
        {
            Table t = new Table("test");
            Dictionary<string, string> st = new Dictionary<string, string>();
            st.Add("name", "text");
            st.Add("age","int");
            t.SetColumms(st);
            //t.AddColumm("name");
            //t.AddColumm("test");

            List<string> l = new List<string>();
            l.Add("Danis");
            l.Add("29");
            t.AddRecord(l);
            t.AddRecord(l);
            t.Save();            

            TableHandler th = new TableHandler();
            th.SetTable(t);
            List<string> l2 = new List<string>();
            l2.Add("Ivan");
            l2.Add("49");
            th.UpdateValueByFiled("name", "Danis", l2);
            th.DeleteRecordByFiled("name", "Danis");
            t.Save();

            Table t2 = new Table("test");
            t2.Load();
            Console.ReadKey();
        }
    }

     

    
}
