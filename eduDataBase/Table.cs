﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace eduDataBase
{
    public class Table
    {
        public string Name { get; set; }

        // table structure
        public Dictionary<string, string> Struct;

        // data
        public List<List<string>> Data;

        public Table(string name)
        {
            Struct = new Dictionary<string, string>();            
            Data = new List<List<string>>();
            Name = name;
        }

        public void SetColumms(Dictionary<string, string> t)
        {
            Struct = t;
        }
        
        public bool AddRecord(List<string> values)
        {
            if (values.Count != Struct.Count)
                return false;
            Data.Add(values);
            return true;
        }
        public void Save()
        {
            string dirName = Name + "\\";

            // flush table folder
            if (Directory.Exists(dirName))
            {
                List<string> t = Directory.GetFiles(dirName).ToList();
                foreach (var file in t)
                    File.Delete(file);
                Directory.Delete(dirName);
            }

            // create table folder
            Directory.CreateDirectory(dirName);

            // write table struct
            string s = "";
            foreach (var item in Struct)            
                s += item.Key + "|" + item.Value+"|";            
            
            File.WriteAllText(dirName + "struct.txt", s);

            //write table info
            File.WriteAllText(dirName + "info.txt", Data.Count.ToString());

            // write table data
            for (int i = 0; i < Data.Count(); i++)
            {
                File.WriteAllText(dirName + i.ToString() + ".txt", String.Join("|", Data[i]));
            }
        }
        public void Load()
        {
            string dirName = Name + "\\";
            // get struct
            string[] s = File.ReadAllText(dirName + "struct.txt").Split('|');
            for (int i = 0; i < s.Length-1; i+=2)
            {
                Struct.Add(s[i],s[i+1]);
            }

            // get info
            int count = Convert.ToInt32(File.ReadAllText(dirName + "info.txt"));

            // get data
            for (int i = 0; i < count; i++)
                Data.Add(File.ReadAllText(dirName + i.ToString() + ".txt").Split('|').ToList());
        }
    }
}
