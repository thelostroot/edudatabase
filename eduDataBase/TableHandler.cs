﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eduDataBase
{
    public class TableHandler
    {
        Table _cur;

        public void SetTable(Table t)
        {
            _cur = t;
        }

        private int GetFieldIndex(string fieldName)
        {
            int fieldIndex = 0;
            foreach (var col in _cur.Struct)
            {
                if (col.Key == fieldName)
                    break;
                fieldIndex++;
            }
            return fieldIndex;
        }

        // UPDATE table SET (recordValue) WHERE fieldName=fieldValue
        public bool UpdateValueByFiled(string fieldName, string fieldValue, List<string> recordValue)
        {
            // get field index by field name
            int fieldIndex = this.GetFieldIndex(fieldName);            

            if (fieldIndex == 0)
                return false;

            for (int i = 0; i < _cur.Data.Count; i++)
            {
                if (_cur.Data[i][fieldIndex] == fieldValue)
                    _cur.Data[i] = recordValue;

            }
            return true;
        }

        // DELETE FROM table WHERE fieldName=fieldValue
        public bool DeleteRecordByFiled(string fieldName, string fieldValue)
        {
            // get field index by field name
            int fieldIndex = this.GetFieldIndex(fieldName);

            if (fieldIndex == 0)
                return false;

            for (int i = 0; i < _cur.Data.Count; i++)
            {
                if (_cur.Data[i][fieldIndex] == fieldValue)
                    _cur.Data.RemoveAt(i);

            }
            return true;
        }

        // SELECT * FROM table
        public List<List<string>> GetAll()
        {
            return _cur.Data;
        }
    }
}
